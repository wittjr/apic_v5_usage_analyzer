const fs = require('fs')
const got = require('got')
const path = require('path')
const JSONStream = require('JSONStream')

async function unserializeJSON(outputFile) {
  var transformStream = JSONStream.parse("*");
  var inputStream = fs.createReadStream(outputFile);
  let return_data = []
  await new Promise(waiting => inputStream
    .pipe(transformStream)
    .on(
      'error',
      (error) => {
        console.error('Error processing file: ' + outputFile + '\nMessage: ' + error.message)
        process.exit(1)
      }
    )
    .on(
      'data',
      (data) => {
        return_data.push(data);
      }
    )
    .on(
      'end',
      waiting
    )
  )
  return return_data
}

async function serializeJSON(data, outputFile) {
  var transformStream = JSONStream.stringify();
  var outputStream = fs.createWriteStream(outputFile);
  transformStream.pipe(outputStream);
  data.forEach(transformStream.write);
  transformStream.end();
  await new Promise(waiting => outputStream
    .on(
      'error',
      (error) => {
        console.error(error)
        console.error('Error creating file: ' + outputFile + '\nMessage: ' + error.message)
        fs.unlinkSync(outputFile)
        process.exit(1)
      }
    )
    .on(
      'finish',
      waiting
    )
  )
}

async function getEvents(gotInstance, props, beginDate, endDate) {
  let records = []

  try {
    const response = await gotInstance('v1/orgs/' + props.org + '/environments/' + props.catalog + '/events', {
      searchParams: {
        limit: props.recordLimit,
        fields: 'datetime,timestamp,apiName,apiVersion,appName,planId,planName,planVersion,productName,productVersion,devOrgName,resourceId,uriPath,queryString,statusCode,productTitle,resourcePath,clientId',
        after: beginDate + 'T00:00:00.000',
        before: endDate + 'T00:00:00.000'
      }
    })

    let events = JSON.parse(response.body)

    for (let j = 0; j < events.calls.length; j++) {
      if (!['401'].includes(events.calls[j].statusCode) && events.calls[j].planId != '') {
        let record = events.calls[j]
        records.push(record)
      }
    }

    while (events.calls.length == props.recordLimit) {
      events = await getEventsPage(gotInstance, events.nextHref)
      for (let j = 0; j < events.calls.length; j++) {
        if (!['401'].includes(events.calls[j].statusCode) && events.calls[j].planId != '') {
          let record = events.calls[j]
          records.push(record)
        }
      }
    }

  } catch (e) {
    console.error(e)
    if (e.options) {
      console.error(e.options.url.href)
    }
  }

  return records
}

async function getEventsPage(gotInstance, url) {
  try {
    const response = await gotInstance(url, {
      prefixUrl: ''
    })
    return JSON.parse(response.body)
  } catch (e) {
    console.error(e)
    if (e.options) {
      console.error(e.options.url.href)
    }
  }
}

async function getProducts(gotInstance, org, catalog) {
  try {
    const response = await gotInstance('v1/orgs/' + org + '/environments/' + catalog + '/products', {
      searchParams: {
        expand: true,
      }
    })
    return JSON.parse(response.body)
  } catch (e) {
    console.error(e)
    if (e.options) {
      console.error(e.options.url.href)
    }
  }
}

async function getSubscriptions(gotInstance, org, catalog) {
  try {
    const response = await gotInstance('v1/orgs/' + org + '/environments/' + catalog + '/subscriptions')
    return JSON.parse(response.body)
  } catch (e) {
    console.error(e)
    if (e.options) {
      console.error(e.options.url.href)
    }
  }
}

async function main() {
  let propsFile = 'properties'
  var input = process.argv.slice(2)
  if (input.length > 0) {
    propsFile = input[0]
  }
  const props = JSON.parse(fs.readFileSync(propsFile))

  const apicInstance = got.extend({
    prefixUrl: 'https://' + props.host,
    https: {
      rejectUnauthorized: false
    },
    headers: {
      authorization: 'Basic ' + Buffer.from(props.username + ':' + props.password).toString('base64')
    }
  })

  const productFile =  props.dataDirectory + path.sep + 'products-' + props.org + '-' + props.catalog + '.json'
  let products = []
  if (fs.existsSync(productFile) && (fs.statSync(productFile).mtime.toDateString() == (new Date()).toDateString())) {
    console.log('File ' + productFile + ' was modified today, not checking again')
    products = await unserializeJSON(productFile)
  } else {
    products = await getProducts(apicInstance, props.org, props.catalog)
    await serializeJSON(products, productFile)
  }

  const subscriptionFile = props.dataDirectory + path.sep + 'subscriptions-' + props.org + '-' + props.catalog + '.json'
  let subscriptions = []
  if (fs.existsSync(subscriptionFile) && (fs.statSync(subscriptionFile).mtime.toDateString() == (new Date()).toDateString())) {
    console.log('File ' + subscriptionFile + ' was modified today, not checking again')
    subscriptions = await unserializeJSON(subscriptionFile)
  } else {
    subscriptions = await getSubscriptions(apicInstance, props.org, props.catalog)
    await serializeJSON(subscriptions, subscriptionFile)
  }

  let apiCallCounts = {}
  for (let i = 0; i < products.length; i++) {
    let product = products[i]
    let apiNames = {}
    for (let j=0; j < product.dependents.APIVERSION.length; j++) {
      let currentEntry = product.dependents.APIVERSION[j]
      apiNames[currentEntry.apiName + ':' + currentEntry.apiVersion] = currentEntry.apiTitle
    }
    let productId = product.productName + ':' + product.productVersion + ':'
    let planNames = Object.keys(product.document.plans)
    for (let j = 0; j < planNames.length; j++) {
      let currentPlan = product.document.plans[planNames[j]]
      if (currentPlan.apis && Object.keys(currentPlan.apis).length > 0) {
        let planApis = Object.keys(currentPlan.apis)
        for (let k = 0; k < planApis.length; k++) {
          let currentApi = product.document.apis[planApis[k]].name
          let key = productId + planNames[j] + ':' + currentApi
          apiCallCounts[key] = {}
          apiCallCounts[key]['product_title'] = product.productTitle
          apiCallCounts[key]['api_title'] = apiNames[currentApi]
          apiCallCounts[key]['count'] = 0
          apiCallCounts[key]['subscriptions'] = {}
          apiCallCounts[key]['last_call'] = ''
        }
      } else {
        let productApis = Object.keys(product.document.apis)
        for (let k = 0; k < productApis.length; k++) {
          let currentApi = product.document.apis[productApis[k]].name
          let key = productId + planNames[j] + ':' + currentApi
          apiCallCounts[key] = {}
          apiCallCounts[key]['product_title'] = product.productTitle
          apiCallCounts[key]['api_title'] = apiNames[currentApi]
          apiCallCounts[key]['count'] = 0
          apiCallCounts[key]['subscriptions'] = {}
          apiCallCounts[key]['last_call'] = ''
        }
      }
    }
  }

  for (let i = 0; i < subscriptions.length; i++) {
    let subscription = subscriptions[i]
    let keys = Object.keys(apiCallCounts)
    for (let j = 0; j < keys.length; j++) {
      if (keys[j].startsWith(subscription.plan.id + ':')) {
        let subscriptionKey = subscription.consumerOrg.name + ':' + subscription.application.appName
        apiCallCounts[keys[j]].subscriptions[subscriptionKey] = {}
        apiCallCounts[keys[j]].subscriptions[subscriptionKey]['count'] = 0
        apiCallCounts[keys[j]].subscriptions[subscriptionKey]['last_call'] = ''
        apiCallCounts[keys[j]].subscriptions[subscriptionKey]['uris'] = {}
      }
    }
  }

  subscriptions = undefined
  products = undefined

  if (props.processAnalytics) {
    let now = Date.now()
    const oneDay = 24*60*60*1000

    let startDay = 1
    if (props.includeToday) {
      startDay = 0
    }
    for (let i = startDay; i < props.days; i++) {
      let fromDay = new Date(now - (i * oneDay))
      let from = fromDay.getFullYear() + '-' + ('0' + (fromDay.getMonth() + 1)).slice(-2) + '-' + ('0' + fromDay.getDate()).slice(-2)
      let nextDay = new Date(now - ((i - 1) * oneDay))
      let to = nextDay.getFullYear() + '-' + ('0' + (nextDay.getMonth() + 1)).slice(-2) + '-' + ('0' + nextDay.getDate()).slice(-2)

      let outputFile = props.dataDirectory + path.sep + 'events-' + from + '.json'
      let summaryFile = props.dataDirectory + path.sep + 'events-summary-' + from + '.json'
      let recordsLoaded = false
      let records = []

      let daySummary = {}
      if (fs.existsSync(summaryFile)) {
          console.log('File ' + summaryFile + ' already exists, skipping')
          daySummary = JSON.parse(fs.readFileSync(summaryFile))
      } else {
        if (fs.existsSync(outputFile)) {
          if (fs.statSync(outputFile).mtime.toDateString() != fromDay.toDateString()) {
            console.log('File ' + outputFile + ' already exists, skipping')
            records = await unserializeJSON(outputFile)
            recordsLoaded = true
          } else if (fs.statSync(outputFile).mtime.toDateString() == fromDay.toDateString() && (new Date(now)).toDateString() == fromDay.toDateString()) {
            console.log('File ' + outputFile + ' already exists and was modified today, skipping')
            records = await unserializeJSON(outputFile)
            recordsLoaded = true
          }
        }
        if (!recordsLoaded) {
          records = await getEvents(apicInstance, props, from, to)
          await serializeJSON(records, outputFile)
        }

        for (let j = 0; j < records.length; j++) {
          let record = records[j]
          let key = record.planId + ':' + record.apiName + ':' + record.apiVersion
          if (!(key in daySummary)) {
            daySummary[key] = {}
            daySummary[key]['count'] = 0
            daySummary[key]['last_call'] = ''
            daySummary[key]['subscriptions'] = {}
          }
          daySummary[key]['count'] += 1
          if (daySummary[key]['last_call'] == '') {
            daySummary[key]['last_call'] = record.datetime
          }
          let subscription_key = record.devOrgName + ':' + record.appName
          if (!(subscription_key in daySummary[key].subscriptions)) {
            daySummary[key].subscriptions[subscription_key] = {}
            daySummary[key].subscriptions[subscription_key]['count'] = 0
            daySummary[key].subscriptions[subscription_key]['last_call'] = ''
            daySummary[key].subscriptions[subscription_key]['uris'] = {}
          }
          daySummary[key].subscriptions[subscription_key]['count'] += 1
          if (daySummary[key].subscriptions[subscription_key]['last_call'] == '') {
            daySummary[key].subscriptions[subscription_key]['last_call'] = record.datetime
          }
          if (record.uriPath in daySummary[key].subscriptions[subscription_key]['uris']) {
            daySummary[key].subscriptions[subscription_key]['uris'][record.uriPath]['count'] += 1
          } else {
            daySummary[key].subscriptions[subscription_key]['uris'][record.uriPath] = {}
            daySummary[key].subscriptions[subscription_key]['uris'][record.uriPath]['count'] = 1
            daySummary[key].subscriptions[subscription_key]['uris'][record.uriPath]['last_call'] = record.datetime
          }
        }
        fs.writeFileSync(summaryFile, JSON.stringify(daySummary))
      }

      let record_keys = Object.keys(daySummary)
      for (let j=0; j<record_keys.length; j++) {
        let key = record_keys[j]
        if (!(key in apiCallCounts)) {
          apiCallCounts[key] = {}
          apiCallCounts[key]['count'] = daySummary[key].count
          apiCallCounts[key]['last_call'] = daySummary[key].last_call
          apiCallCounts[key]['subscriptions'] = {}
        } else {
          apiCallCounts[key]['count'] += daySummary[key].count
          if (apiCallCounts[key]['last_call'] == '') {
            apiCallCounts[key]['last_call'] = daySummary[key].last_call
          }
        }
        let subscription_keys = Object.keys(daySummary[key].subscriptions)
        for (let k=0; k<subscription_keys.length; k++) {
          let summary_subscription_key = subscription_keys[k]
          let subscription_key = summary_subscription_key
          if (!(subscription_key in apiCallCounts[key].subscriptions)) {
            subscription_key = 'missing_sub - ' + subscription_key
          }
          if (!(subscription_key in apiCallCounts[key].subscriptions)) {
            apiCallCounts[key].subscriptions[subscription_key] = {}
            apiCallCounts[key].subscriptions[subscription_key]['count'] = daySummary[key].subscriptions[summary_subscription_key].count
            apiCallCounts[key].subscriptions[subscription_key]['last_call'] = daySummary[key].subscriptions[summary_subscription_key].last_call
            apiCallCounts[key].subscriptions[subscription_key]['uris'] = daySummary[key].subscriptions[summary_subscription_key].uris
          } else {
            apiCallCounts[key].subscriptions[subscription_key]['count'] += daySummary[key].subscriptions[summary_subscription_key].count
            if (apiCallCounts[key].subscriptions[subscription_key]['last_call'] == '') {
              apiCallCounts[key].subscriptions[subscription_key]['last_call'] = daySummary[key].subscriptions[summary_subscription_key].last_call
            }
            let uri_paths = Object.keys(daySummary[key].subscriptions[summary_subscription_key].uris)
            for (let l=0; l<uri_paths.length; l++) {
              let uri_path = uri_paths[l]
              if (uri_path in apiCallCounts[key].subscriptions[subscription_key]['uris']) {
                apiCallCounts[key].subscriptions[subscription_key]['uris'][uri_path]['count'] += daySummary[key].subscriptions[summary_subscription_key].uris[uri_path].count
              } else {
                apiCallCounts[key].subscriptions[subscription_key]['uris'][uri_path] = {}
                apiCallCounts[key].subscriptions[subscription_key]['uris'][uri_path]['count'] = daySummary[key].subscriptions[summary_subscription_key].uris[uri_path].count
                apiCallCounts[key].subscriptions[subscription_key]['uris'][uri_path]['last_call'] = daySummary[key].subscriptions[summary_subscription_key].uris[uri_path].last_call
              }
            }
          }
        }
      }
    }
  }

  const reportFile = props.dataDirectory + path.sep + 'report-' + props.org + '-' + props.catalog + '.json'
  fs.writeFileSync(reportFile, JSON.stringify(apiCallCounts))
}

try {
  main()
} catch (e) {
  console.error(e)
}
