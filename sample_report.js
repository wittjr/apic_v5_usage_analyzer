const fs = require('fs')
const path = require('path')

async function main() {
  let propsFile = 'properties'
  var input = process.argv.slice(2)
  if (input.length > 0) {
    propsFile = input[0]
  }
  const props = JSON.parse(fs.readFileSync(propsFile))

  const reportFile = props.dataDirectory + path.sep + 'report-' + props.org + '-' + props.catalog + '.json'
  const reportData = JSON.parse(fs.readFileSync(reportFile))

  const outputFile = props.dataDirectory + path.sep + 'report-' + props.org + '-' + props.catalog + '.csv'
  let header_row = 'Product,Product Title,Product Version,Plan,API,API Title,APIVersion,Consumer Org,App\n'
  if (props.processAnalytics) {
    header_row = 'Product,Product Title,Product Version,Plan,API,API Title,API Version,Calls,Last Call,Consumer Org,App,Calls,Last Call,URI,Calls,Last Call\n'
  }
  fs.writeFileSync(outputFile, header_row)
  Object.keys(reportData).forEach(key => {
    let row = key.split(':')
    row.splice(1, 0, reportData[key].product_title)
    row.splice(5, 0, reportData[key].api_title)
    if (props.processAnalytics) {
      row.push(reportData[key].count)
      row.push(reportData[key].last_call)
    }
    let sub_keys = Object.keys(reportData[key].subscriptions)
    if (sub_keys.length == 0) {
      fs.appendFileSync(outputFile, row.join(',') + '\n')
    } else {
      sub_keys.forEach(sub => {
        let row_sub = row.concat(sub.split(':'))
        if (props.processAnalytics) {
          row_sub.push(reportData[key].subscriptions[sub].count)
          row_sub.push(reportData[key].subscriptions[sub].last_call)
        }
        let uri_keys = Object.keys(reportData[key].subscriptions[sub].uris)
        if (uri_keys.length == 0) {
          fs.appendFileSync(outputFile, row_sub.join(',') + '\n')
        } else {
          uri_keys.forEach(uri => {
            let row_sub_uri = row_sub.concat([uri])
            row_sub_uri.push(reportData[key].subscriptions[sub].uris[uri].count)
            row_sub_uri.push(reportData[key].subscriptions[sub].uris[uri].last_call)
            fs.appendFileSync(outputFile, row_sub_uri.join(',') + '\n')
          })
        }
      })
    }
  });
}

try {
  main()
} catch (e) {
  console.error(e)
}
